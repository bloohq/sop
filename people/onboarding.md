# Onboarding

The goal of onboarding new team members is to:

1. Get them up to speed as quickly as possible so they can be productive and contribute.
2. Make them feel welcome!

We have a[ people project in Blue ](https://beta.app.blue.cc/company/blue/projects/people/todo)where we keep important information for each team member. Key information that we need to capture for each new team member:

* **Banking information:** Where do we pay the salary?
* **Copy of ID:** Copy of government-issued ID
* **Emergency Family/Spouse Contact Details:** Details of Next of Kin
* **Address:** What is their home address?
* **Salary Information:** How much is this team member earning?
* **Asset Information:** What assets (i.e. computers) have been provided to the team member?

In addition, we provision access to:

* Corporate Email — This is via our Google Workspace subscription. Our default naming convention is {fname}@blue.cc
* Blue Account — There should be an invite to Blue already in their inbox when they first check their email.&#x20;
* Gitlab — Repo Access via Gitlab if the team member is an engineer.
* Telegram — Chat Access to Blue Team, and if the team member is an engineer, then Blue Dev group as well.

Team leads should take the new team member out for a welcome lunch in their first week if they are not working remotely.&#x20;

