# Offboarding

## Notice Period

The mutual notice period is one month for team members who have passed the 3-month probationary period. There is no notice period for team members who are still on probation.&#x20;



## Offboarding Checklist

* [ ] Remove from Google Workspace
* [ ] Remove from Telegram
* [ ] Remove from Blue
* [ ] Remove Access to Gitlab

## Last Salary Calculations

To avoid any confusion, this is the calculation we use:&#x20;

$$
Last Salary = (M/W) x (D)
$$

Where:

* M = **Monthly** salary
* W = number of **Working days** of the month, Mondays to Fridays
* D = number of **Days worked**

As an example, let's say:&#x20;

* Your Monthly salary = **$1000**
* Your last day is effective on March = **21 days** (Mondays to Fridays)
* Your last working day is March 17, 2019. It happens to be a Sunday, so the last working day is considered as March 15, Friday. That would make it **11 days** in March.&#x20;

So:

Last Salary = ($1000/21 days) = $47.62

Last Salary = $47.62 x 11 days

**Last Salary = $523.82**

If, on the other hand, you have any outstanding reimbursements or owe Blue any sums of money, these will also be considered in the calculation of your last pay — i.e. added or deducted.&#x20;

Your Last Salary will be paid to you on the scheduled payday, assuming you have correctly completed your turnover/handover to your Team Lead or Department Head and they confirm that everything has been received in good order.&#x20;
