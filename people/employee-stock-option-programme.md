# Employee Stock Option Programme

Blue has an ESOP programme to incentivise long-term thinking and growth within the organisation. The tracking of the ESOP is done via this [Google Spreadsheet](https://docs.google.com/spreadsheets/d/1oLqmHac\_6wyvEf\_liGA7ZnXrMrMpHMMQcBLuFbgWlDo/edit?usp=sharing) that is viewable by management only.&#x20;

Share Options are issued each January, and are typically given in tranches of two to three years.&#x20;

The details of the Stock Option Plan are in this PDF:

{% file src="../.gitbook/assets/Bloo Inc - Stock Option Plan 2024 copy.pdf" %}

