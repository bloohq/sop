# Website Sitemap

This is the live Blue website sitemap with URL references:

- [Blue.cc](https://blue.cc)
    - [About](https://blue.cc/about)
    - [Blog](https://blue.cc/blog)
        - [Deep-work](https://blue.cc/blog/deep-work)
        - [Great-teamwork](https://blue.cc/blog/great-teamwork)
        - [Hello](https://blue.cc/blog/hello)
        - [Problem-overworking](https://blue.cc/blog/problem-overworking)
        - [The-future-is-blue](https://blue.cc/blog/the-future-is-blue)
    - [Legal](https://blue.cc/legal)
        - [Privacy-policy](https://blue.cc/legal/privacy-policy)
        - [Terms](https://blue.cc/legal/terms)
    - [Partners](https://blue.cc/partners)
    - [Platform](https://blue.cc/platform)
        - [Automations](https://blue.cc/platform/automations)
        - [Changelog](https://blue.cc/platform/changelog)
        - [Custom-fields](https://blue.cc/platform/custom-fields)
        - [Database](https://blue.cc/platform/database)
        - [Import-export](https://blue.cc/platform/import-export)
        - [Integrations](https://blue.cc/platform/integrations)
        - [Kanban-board](https://blue.cc/platform/kanban-board)
        - [Project-management](https://blue.cc/platform/project-management)
        - [Records](https://blue.cc/platform/records)
        - [Roadmap](https://blue.cc/platform/roadmap)
        - [Security-scalability](https://blue.cc/platform/security-scalability)
        - [Api](https://blue.cc/platform/api)
        - [Timeline-gantt-charts](https://blue.cc/platform/timeline-gantt-charts)
        - [Calendar](https://blue.cc/platform/calendar)
        - [Lists](https://blue.cc/platform/lists)
        - [Map](https://blue.cc/platform/map)
        - [Activity-audit-trail](https://blue.cc/platform/activity-audit-trail)
        - [User-permissions](https://blue.cc/platform/user-permissions)
        - [Wiki-docs](https://blue.cc/platform/wiki-docs)
        - [File-management](https://blue.cc/platform/file-management)
        - [Chat](https://blue.cc/platform/chat)
        - [Forms](https://blue.cc/platform/forms)
        - [Dashboards](https://blue.cc/platform/dashboards)
        - [Desktop-mobile-apps](https://blue.cc/platform/desktop-mobile-apps)
    - [Pricing](https://blue.cc/pricing)
    - [Reviews](https://blue.cc/reviews)
    - [Solutions](https://blue.cc/solutions)
        - [Best-project-management-software-for-non-profits](https://blue.cc/solutions/best-project-management-software-for-non-profits)
        - [Best-project-management-software-for-solopreneurs](https://blue.cc/solutions/best-project-management-software-for-solopreneurs)
        - [Operations](https://blue.cc/solutions/operations)
        - [Professional-services](https://blue.cc/solutions/professional-services)
        - [Project-management](https://blue.cc/solutions/project-management)
        - [Sales-crm](https://blue.cc/solutions/sales-crm)
        - [Real-estate](https://blue.cc/solutions/real-estate)
        - [Local-government](https://blue.cc/solutions/local-government)
        - [Construction](https://blue.cc/solutions/construction)
        - [Agencies](https://blue.cc/solutions/agencies)
        - [Law-firms](https://blue.cc/solutions/law-firms)
        - [Human-resources](https://blue.cc/solutions/human-resources)
        - [It](https://blue.cc/solutions/it)
        - [Service-tickets](https://blue.cc/solutions/service-tickets)
        - [Marketing](https://blue.cc/solutions/marketing)
        - [Finance](https://blue.cc/solutions/finance)
    - [Support](https://blue.cc/support)
    - [Rewards](https://blue.cc/rewards)

## Python Code

This is the Python code to generate the sitemap:

```
import requests
from bs4 import BeautifulSoup

def download_sitemap(url):
    response = requests.get(url)
    response.raise_for_status()
    return response.text

def parse_sitemap(sitemap_xml, base_domain):
    soup = BeautifulSoup(sitemap_xml, 'xml')
    urls = soup.find_all('loc')
    page_structure = {base_domain: {}}  # Initialize with base domain

    def insert_into_structure(structure, parts):
        if len(parts) == 0:  # Base case: No more parts
            return
        if parts[0] not in structure:
            structure[parts[0]] = {}  # Initialize a new sub-structure
        insert_into_structure(structure[parts[0]], parts[1:])  # Recursive call for the next part

    for url in urls:
        url_text = url.text.strip()
        url_parts = url_text.replace('https://www.', '').replace('http://www.', '').replace('https://', '').replace('http://', '').split('/')[1:]  # Split path
        insert_into_structure(page_structure[base_domain], url_parts)

    return page_structure

def capitalize_first_letter(text):
    """Capitalizes the first letter of each segment in a URL path."""
    return '/'.join(segment.capitalize() for segment in text.split('/'))

def generate_markdown(structure, base_url, parent_path='', output_file='output.md', prefix=''):
    lines = []
    for page, subpages in structure.items():
        # Adjust for whether this is the base domain or a subpage
        if page != base_url:
            page_path = f"{parent_path}/{page}" if parent_path else page
            page_display = capitalize_first_letter(page)
            full_url = f"https://{base_url}/{page_path}"
        else:
            page_display = base_url.capitalize()
            full_url = f"https://{base_url}"
            page_path = ''

        line = f"{prefix}- [{page_display}]({full_url})"
        lines.append(line)
        
        if subpages:  # If there are subpages, recursively generate their lines with increased indentation
            lines.extend(generate_markdown(subpages, base_url, page_path, output_file=None, prefix=prefix + "    "))
    
    if output_file:
        with open(output_file, 'w') as f:
            f.write('\n'.join(lines))
    else:
        return lines

base_domain = 'blue.cc'
sitemap_url = f'https://{base_domain}/sitemap.xml'  # Your sitemap URL
sitemap_xml = download_sitemap(sitemap_url)
page_structure = parse_sitemap(sitemap_xml, base_domain)
generate_markdown(page_structure, base_domain)

print(f"Markdown file generated.")



```
