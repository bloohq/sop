# Product Screenshots

This is how we create high-quality screenshots for our website.&#x20;

We use two key tools:

* [**Cleanshot**](https://cleanshot.com/) — This is used to create screenshots of the Blue interface.
* [**TinyPNG**](https://tinypng.com/) —  This is used to compress the files to ensure fast website loading speed.

## Process:&#x20;

1. **Start with Desktop App:** Don't screenshot from the browser; use the Desktop App instead so we do not have an additional interface around Blue.&#x20;
2. **Sizing:** Aim for a screenshot size of approximately 3000px by 1800px. This size is ideal for maintaining high resolution while ensuring the images are not excessively large for web use. The screenshot size ratio is approximately 1.67:1, commonly called 5:3.
3. **Capture with CleanShot:** Launch CleanShot to capture images of the Blue interface. After capturing, image editing options appear automatically.
4. **Edit Image:** Adjust the Padding to 77 pixels for uniform borders, enable "Auto Balance" for optimal padding, set Corner Radius to 3 pixels for slightly rounded edges, and choose a background color that matches the product (e.g., white, transparent, or #00a2d2 blue).
5. **Highlight Features:** Use the Highlight tool to emphasize specific product features, adjusting the highlight box to desired areas.
6. **Review and Save:** Ensure the edited image meets quality standards and effectively showcases the product, then click "Save".
7. **Compress with TinyPNG:** Upload the saved image to TinyPNG to compress the file size without significantly compromising image quality. This step ensures faster website loading speeds.
8. **Name the File:** Name the image file descriptively for SEO, incorporating relevant keywords that reflect the product features or interface being showcased. Use hyphens to separate words (e.g., "blue-interface-highlighted-feature.png").

{% hint style="info" %}
**Editing Previously Captured Screenshots**\
If you need to change a previously saved image, go to the CleanShot Capture History. Locate the image you want to edit and click the "Edit" button to make any necessary adjustments.
{% endhint %}

\
