# Conversion Rates

#### **Inbound Conversion Rates:**

* Referrals: Convert to customers at a rate of approximately 60-90%.
* Content downloads and trade show/webinar attendees: Convert at about 3-5%.
* High intent form fills (e.g., "book a demo"): Conversion rate around 20-35%.

#### **Outbound Conversion Rates:**

* For cold businesses (those who have never heard of you): Expect a conversion rate of about 3-5% from lead to customer.
* For reactivation businesses (those you’ve failed to sell to in the past): An acceptable conversion rate is around 1-2%.
