# Introduction

{% hint style="warning" %}
Update this text below
{% endhint %}

B2B Sales is simple. Generate some interest. Help those interested to buy your product.

At its most basic level, it looks like:&#x20;

Lead >> Opportunity >> Customer

* Leads are _individuals_ who have shown interest.&#x20;
* Opportunities are _companies_ where the seller has reciprocated the interest.
* Customers are paying you money.
