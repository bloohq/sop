# Discovery Calls

A discovery call is a strategic sales conversation to uncover a potential buyer's business needs, challenges, and objectives to tailor the sales approach effectively. **We have to diagnose the problem before prescribing a solution.**&#x20;

Discovery calls typically last 30 minutes or less. Long enough to be useful, and short enough not to waste anyone's time.

{% hint style="warning" %}
**This is not a demo.**&#x20;

We are _not_ trying to show all the features in Blue or "sell" the platform. We are trying to understand the prospect's business and their pain points deeply.&#x20;

The key question we must ask ourselves is: _Can implementing Blue solve these pain points?_
{% endhint %}

This approach enables us to clearly demonstrate Blue's value, presenting our solution as reasonable and necessary for the customer.&#x20;

Remember that we [sell painkillers, not vitamins or candy:](../ideal-customer-profile.md)

* **Painkiller —** Solutions that address an organisation's most urgent and intense problems. These are desperately needed, and they were needed yesterday.&#x20;
* **Vitamins —** Beneficial features that are nice-to-have but not essential. While they can add long-term value, vitamins don't solve critical urgent needs directly.
* **Candy —** Fun or delightful features that keep customers engaged but aren't critical to solving core problems. These can help differentiate our platform, but will not sway buying decisions.&#x20;

## What It Takes To Buy.

For a prospect to buy, they have to answer three fundamental questions:

1. Why should I buy anything?
2. Why should I buy from you?
3. Why should I buy now?

### Why should I buy anything?

Or, to rephrase it: _"Why not stick with the status quo?"_

This is a legitimate question. Is it that serious if the prospect has a pain point and has not yet bothered addressing it?&#x20;

For a mid-sized business (30 seats), Blue can cost $84/user/year for a total of $2,500/year.&#x20;

Is the current pain costing just one hour per month? Then, a rational prospect would likely decide _against_ implementing Blue.&#x20;

We have to become experts in the problems we solve. We have to be able to get to the root causes and see if the stated problem is _not_ just one hour a month but is worth several people's salary, additional lost revenue, and unhappy end customers. This is a solid pain point that can be quantified as a $25,000 to $1,000,000 problem.&#x20;

Viewed through this perspective, the $2,500 yearly subscription fee is not only justified, but great value.&#x20;

Before moving forward in the sales process, it's crucial to confirm that the buyer doesn't have lingering doubts or confusion, particularly regarding the fundamental question of _"Why should I buy anything?"_

**​**

**Why should I buy from you?**

Pain points often remain remarkably constant, but the solutions change.

Let's take one of the oldest problems, the question: _"How do I get there?"_

This was initially solved by looking at the stars and other natural features such as mountains and rivers. Then we invented maps, compasses, the Marine Chronometer, TomToms, and finally, smartphones. Who knows what will come next?

Each of these solutions solves the pain in a very different way, and some of them do it better than others. Using an iPhone with Google Maps is great, but what if you stay in the wild without electricity for months? Suddenly, a paper map, a compass, and the ability to use stars and natural features starts looking very attractive.&#x20;

Likewise, Blue solves certain pain points that have multiple _other_ solutions. One of our biggest "competitors" is written sticky notes taped to people's desktops, let alone the [actual software competitors. ](../competitor-benchmarking.md)

We must understand how our prospects work and their critical business objectives, so we can position Blue as the natural choice during an evaluation.&#x20;

Prospects must be clear as to why they should choose Blue and not any alternatives.&#x20;

**​**

**Why should I buy now?**

Humans naturally prefer the status quo to avoid risk.&#x20;

Therefore, to motivate a prospect to act promptly, it's essential for us to provide a compelling reason, often financially based, answering the "Why buy now?" question.&#x20;

Effective strategies include aligning the purchase with critical business deadlines. These can be financial quarters, company objectives (OKRs), or high-level project timelines.

Another good strategy is to highlight the daily/weekly/monthly cost of inaction. This approach involves quantifying the ongoing losses or missed opportunities that occur by not utilizing your product or service. For instance, if they are considering using Blue as a CRM, what would a 20% increase in their close rate equate to? This tangible representation of loss can make the cost of waiting more real and urgent for the prospect, compelling them to consider the immediate benefits of making a purchase decision.

## Agenda

This is a high-level agenda for the discovery call.&#x20;

* **5 minutes:** Introduction, overview of agenda, small talk.
* **15 minutes:** Uncovering the pain. We have to start to answer the first two questions. _"Why buy anything?"_ and _"Why buy now?"_
* **5 minutes:** Start to answer, _"Why buy from you"?_ buy showing **one** workflow
* **5 minutes:** Next steps and BAMFAM. We should try and book a meeting with additional the prospect and additional colleagues within one week.&#x20;

{% hint style="warning" %}
**Importance of BAMFAM**

BAMFAM stands for _"Book a meeting, from a meeting"._&#x20;

This is crucial because if you don't secure a follow-up meeting during the current one, you risk losing momentum and the prospect's engagement — they may lose interest or get sidetracked by other priorities, reducing the likelihood of progressing the sales conversation.&#x20;
{% endhint %}



\












