# The Importance of Customers

## What is a Customer?

Let's start by defining the word "Customer". A customer is an organisation that pays Blue money for our platform. This is important. **We should not prematurely label leads or potential opportunities as customers.**&#x20;

Recognizing an organisation as a customer only _after_ a financial exchange has occurred ensures a realistic and honest assessment of the sales process. It prevents the misconception that a sale has been made when, in fact, the process might still be in the negotiation or consideration stages.

## Users

Moving on to the concept of users, it's important to distinguish them from customers. While our customers are the organizations that pay for our platform, users are the individuals within these organizations who actually engage with and utilize our platform. They are the end-users working for, often representing, the customer organization.

This distinction is critical for several reasons.&#x20;

Firstly, users' needs and feedback directly impact how our platform evolves, as they interact with it daily. Their experience can influence the organization's decision to continue or expand their relationship with us.&#x20;

Secondly, understanding the difference helps us tailor our support and development efforts. While we might discuss contractual and business terms with our customers, our focus with users is more on usability, features, and technical support.

&#x20;Lastly, recognizing this distinction allows us to communicate and market our platform effectively, as the messaging for decision-makers (our customers) might differ from that for the end-users (users), who require more detailed, functional information about our platform.





* They have a tremendous amount of trust in us.
* Company valuation.&#x20;
  * Typical SaaS Valuation can be 8-10x ARR. This can go as high as 20-25x in certain markets and under certain conditions
  * So even one $7/month user is worth $840 of company value.&#x20;
