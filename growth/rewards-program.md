# Rewards Program

This program aims to incentivise our customers to share their experiences with Blue. By taking action to promote Blue, customers can earn credits that will discount future invoices from Blue.&#x20;

The total that one user can earn for their organisation is $550, but multiple users within one company can take these actions to stack up credits in the rewards program.

## Actions

### Social Media Sharing

| Action                 | Credit ($) |
| ---------------------- | ---------- |
| Facebook Post          | 3          |
| Post to Facebook Group | 5          |
| Linkedin Post          | 3          |
| Twitter Post           | 3          |
| Reddit Post            | 5          |
| Quora Answer           | 7          |

### Content Creation

| Action                         | Credit ($)                      |
| ------------------------------ | ------------------------------- |
| Blog Post Mention              | 20                              |
| Product Tutorial or Case Study | 40                              |
| Newsletter                     | 50+ (Depending on channel size) |
| Podcast Mention                | 30                              |
| Backlink (0-20 Domain Rating)  | 5                               |
| Backlink (20-50 Domain Rating) | 10                              |
| Backlink (50+ Domain Rating)   | 20                              |

### Reviews & Ratings

| Action                        | Credit ($)                       |
| ----------------------------- | -------------------------------- |
| Written Review                | 20                               |
| iOS Mobile App Review         | 15                               |
| Google Play Mobile App Review | 15                               |
| YouTube Review                | 100+ (Depending on channel size) |
| G2 Review                     | 25                               |
| Capterra Review               | 25                               |
| Trustpilot Review             | 25                               |
| AlternativeTO Review          | 25                               |
| SoftwareAdvice Review         | 25                               |
| TrustRadius Review            | 25                               |
| Financesonline Review         | 25                               |
| ProductHunt Review            | 25                               |
