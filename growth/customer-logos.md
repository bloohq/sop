# Customer Logos

On our website, one key goal is building trust and showcasing social proof.&#x20;

Social proof can come from the number of users/customers, awards, and logos of existing customers and users. This is why it is important to periodically scan our user base for well-known brands using Blue.

The steps to do this are straightforward.

Firstly, export all emails of our entire customer base via Metabase. This should be just a one-column CSV file with the header "emails"

Then, use the following Python script to turn this into a list of unique customer domains in Blue.

```python
import pandas as pd

# Load the CSV file
file_path = 'path_to_your_csv_file.csv'  # Replace with your file path
data = pd.read_csv(file_path)

# List of possible column names for email
email_column_variations = ['Email', 'e-mail', 'E-mail', 'EMAIL', 'E-MAIL', 'emails', 'Emails', 'E-mails', 'E-MAILS']

# Find the actual column name used in the CSV
email_column = None
for col in email_column_variations:
    if col in data.columns:
        email_column = col
        break

# Raise error if no email column is found
if not email_column:
    raise ValueError("The CSV file does not have an 'Email' column or any variation of it.")

# Extract domain from each email address
data['Domain'] = data[email_column].str.extract(r'@([\w\.-]+)')

# Identify unique domains
unique_domains = data['Domain'].unique()

# Create a new DataFrame with unique domains
unique_domains_df = pd.DataFrame(unique_domains, columns=['Unique Domains'])

# Save to a new CSV file
output_file = 'unique_domains.csv'
unique_domains_df.to_csv(output_file, index=False)
print(f"Unique domains have been saved to {output_file}")
```

Then, we can manually scan or use AI to find domains of famous/global brands.

{% hint style="info" %}
To find SVG logos of global companies, we can use [WorldVectorLogo ](https://worldvectorlogo.com/)
{% endhint %}



