# Writing Cold Emails



* Don't introduce yourself. — My Name is...and I am from (you are not Eminem)
* Max of 60-70 words
* End with a clear ask.





Good content found on LInkediN that we can use for this:\
\
Here’s the quick tip for a 4-liner email:

1 - Answering your prospect's question about why you are reaching

The first line of your email should be answering the question, “Why me?” This is a trigger that will make the prospect think this email is based on my position.

“I was checking..., I saw that…, etc…

Triggering the prospect’s problem on the first line is a huge gain!

2 - Describe how you solved their problem.

On the second line, tell them how you solved their problem. The format might look like:

“I noticed you might have a problem with Y; we help people do X."

Think about what you can offer that your competitors cannot.

Stand out by saying something about the unique value proposition of your product/service. This way, you can get prospects attention.

3 - Share social proof.

On the third line, tell them how you solved problems for other companies like theirs.

Social proof is the evidence that shows you that you have the expertise and experience to solve problems.

For example, I recently helped ABC Inc., a well-known company in your industry, increase their sales by 25% in just 3 months, and I also worked with XYZ Ltd., a similar-sized company to yours, to reduce their costs by 15% in 6 weeks.

4 - Soft call-to-action

End your email with a soft call to action. These will allow the prospects to engage in a “no-direct meeting/call.”

A call to action you can use is: If we could help you with \[the problem], would that be useful to you?

“Can I send a video >3 min showing how our product/service works?”

Keep it as short as you can; less than 100 words of email performs best.
