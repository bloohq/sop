# Value Proposition

Customers have [options](competitor-benchmarking.md). So, we need to have a _unique_ value proposition to be able to answer the second of the three key questions that customers ask themselves when navigating a buying decision:

1. Why should I buy anything?
2. **Why should I buy from you?**
3. Why should I buy now?

Competitors such as Airtable, Asana, ClickUp, Monday, and Notion have cumulatively raised $3B+. Blue is self-funded, so we will never have even a fraction of these organisations' engineering resources or marketing reach. However, we can use the very fact that we are _not_ venture-funded and turn that to our advantage.

## Value Proposition 1: Simplicity

If an organisation goes out and hires 100s of engineers, you have to give them something to do. This means feature bloat, and often over-complicated features because teams are trying to be clever, not useful.

Once you start down the path of complexity, it's extremely difficult to turn back. Generally, you cannot start removing features.&#x20;

Our advantage is that we do not have 100s of engineers, and so we have to pick and choose our battles carefully.  A feature really has to be a YES to get built. We do less, but _better_.

This means that our product is simpler to use and understand.&#x20;

From a customer's perspective, this means:

1. **Faster Onboarding —** They can set up Blue and get their team on board in _hours_, not days or weeks.&#x20;
2. **Less Training Overhead —** They don't have to run classes on how to use Blue. Teams just _"get"_ it.&#x20;
3. **Higher Chance of Adoption —** Picking the right platform is half the battle. Ensuring usage and adoption is the other half. Blue ensures that _both_ halves are covered.&#x20;

## Value Proposition 2: Affordability

Most of our competitors operate from some of the world's most expensive cities, and their base costs reflect this. In the end, customers pay for all the inflated costs.

Blue offers an 80/20 approach: 80% of their Enterprise feature set at 20% of the price.

This value proposition is especially attractive in the current economic climate, where companies are trying to save costs and ensure they can continue operating.&#x20;

A team of 30 people using Asana using the business edition can expect to pay $10,976/year (30 users  \* $30.49 per user \* 12 months).

The same team would only pay $2,520 (30 users \* $7 per user \* 12 months). Blue is only 22% of the cost of Asana — and that does not even count for Asana's more expensive plans, whose prices are not available on their website.&#x20;

Blue also has a generous amount of[ free user types. ](https://docs.blue.cc/blue-documentation/start-guide/faqs)

From a customer's perspective, this means:

1. **Less Cost —** This is the obvious one. Blue allows customers to keep money in their bank accounts to invest in other business areas.&#x20;
2. **No Penalty for Growth —** With many other tools, you have to move to higher-paid plans as you grow your team, but with Blue, we have one plan with all features regardless of plan size.&#x20;
3. **Only Pay for Core Team —** Invite vendors, clients, and senior stakeholders who only need to comment or view completely free of charge. Not all tools allow this.&#x20;

## Value Proposition 3: Flexibility & Customisation.

The key point to drive home here is that an organisation should not have to change their processes to fit a platform, but the platform should change to fit their processes.  This is important because the way that businesses (i.e. our clients!) differentiate themselves in _their_ markets should be through their unique business processes. After all, if they are just following "best practices" like everyone else, how can they serve their customers any better than their competition?

Blue provides a flexible toolset that does not create a straight-jacket for an organisation. Teams can add custom fields and define their process steps and automations to achieve their business objectives precisely.&#x20;

From a customer's perspective, this means:

1. **Differentiation**— Blue _adapts_ to the unique organisational work processes, not vice versa. This ensures they stand out and serve customers more effectively than their competitors.
2. **Faster & Easier Onboarding** — Organisations do not have to re-engineer their workflows to start using Blue, although it is typically a good time to do so!&#x20;
3. **Flexible Use Cases —** Organisations may start using Blue for one function, then realise that they can use it for various other functions and truly have "one place for everything".&#x20;

## Value Proposition 4: Support

## Value Proposition 4: Support



## To complete:&#x20;



* No Commitment
  * 7 day free trial
  * 30 day money back guarantee
  * No lock in contracts, month to month contracts.&#x20;



* Tested and Robust system
  * Global customer base&#x20;
  * 660+ API calls&#x20;
  * etc
* Constantly improving&#x20;
  * Always working on user feedback
  * Monthly new features
* Multiple Processes in one place, gives an all-in-one view
* Fantastic support
  * Any time 30-min free call
  * AI documentation
  * In-App Helpers/Walkthrough
