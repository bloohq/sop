# Partner Program

We have a partner programme with a 42% revenue share agreement. Why 42? Because that is the answer to the Ultimate Question of Life, The Universe, and Everything.&#x20;

{% embed url="https://www.youtube.com/watch?v=0lngjO5bV2Y" %}

So the revenue share for one user at $7/month is:

* Blue: $4.06
* Referral Partner: $2.94

For a typical Blue customer with 25 users, this works out at $900/year in commissions.&#x20;

$$(25 *7*12)*0.42 = $900$$

Partners can sign up at:

{% embed url="https://partners.blue.cc" %}
Partner Portal for Blue
{% endembed %}

### Coupons

We can offer a $2 discount per user (\~28% discount) so that the price of Blue is $5/month/user to a referral partner's customer base. In this case, the revenue share will be:

* Blue: $3
* Referral Partner: $2

The coupon is tracked, so _any_ sale that uses this coupon will be attributed back to the specific referral partner.&#x20;

### Brand Assets

Partners can access all of our brand assets over at the brand asset page:

{% content-ref url="../brand-assets.md" %}
[brand-assets.md](../brand-assets.md)
{% endcontent-ref %}

### Custom Landing Pages

If there is likely to be high volume (i.e. you have a list with thousands of potential leads), then our team can create a custom landing page that referral partners can use as a call-to-action for emails and their marketing efforts. All sign-ups via this landing page and any other pages on the Blue website will be attributed to the referral partner.

We can integrate [Direct Page Tracking](https://help.rewardful.com/en/articles/5237013-direct-page-tracking) into custom landing pages so that the referral partner does not have to add a **?={partnername}** after the link.&#x20;

### Introduction Email

You can use This sample introduction email when introducing new customers directly. We find that writing introductory emails and having an initial introductory call has the highest conversion rates compared to sending.&#x20;

{% code overflow="wrap" fullWidth="true" %}
```
Hey [Name],

Hope you've been well! I wanted to tell you about Blue - it's a fantastic project management platform I think could be really helpful for your team.

Blue has been around for over 6 years and is used by thousands of organizations globally. It makes managing projects super easy and organized. You can create custom workflows, fields, reports and automations to map your processes perfectly.

Some of the key things:

Boards to visualize workflows
Timelines for scheduling
Options for mobile and desktop
Integrates with your other tools
Analytics and dashboards
I know you've been looking for something to level-up your project management. Blue has a great balance of power plus simplicity.

Let me introduce you to [Founder's Name] to give you a quick demo and show if Blue is a good fit. He's the founder and knows the product inside-out.

No pressure at all — I just want to provide some options that could benefit your team!

```
{% endcode %}

## Cold Email for Partners <a href="#when-do-i-need-to-manually-attribute-a-referred-customer" id="when-do-i-need-to-manually-attribute-a-referred-customer"></a>

We manually send cold-email introductions to potential partners. This can be anyone who speaks about productivity, sales, and process.&#x20;

<pre data-overflow="wrap" data-full-width="true"><code><strong>Subject: {first name or company name} &#x26; Blue or "Collaborate with Blue? Let's Talk, {firstname}
</strong>
Hey {first name},

Manny here, CEO &#x26; Founder of Blue – a user-friendly project and process management system, often used for {use-cases}.

Next month, we're launching our partner program, offering a 42% lifetime revenue share, potentially earning you $1000+ annually per referred customer.

I've seen your {Blog/Youtube Channel, etc} that you've {written/spoken/posted} many times on [topic] — so I believe a partnership with us could be mutually beneficial. 

This is not just an earning opportunity but a chance to bring a valuable tool to your audience. To explore this further, let's have a quick chat. You can book a time that suits you here: [call link].

Looking forward to the possibility of working together.
Best, 
Manny
</code></pre>

{% hint style="info" %}
We need to figure out how to automate this at a larger scale including follow ups.&#x20;
{% endhint %}



## Manual Attribution <a href="#when-do-i-need-to-manually-attribute-a-referred-customer" id="when-do-i-need-to-manually-attribute-a-referred-customer"></a>

Sometimes, a customer will sign up through a partner in a way that Rewardful cannot track. For example, a visitor might discover your product by following a partner link on their smartphone, but then sign up later by visiting your website directly on a laptop.

In these cases, you can connect the Stripe customer to the partner manually so the partner gets commissions on future invoices. Rewardful also lets you generate commissions that the customer has already paid.

How you manually attribute customers to a partner depends on whether you've connected your Stripe account to Rewardful with read-write access (the default) or read-only access.

### 1. Copy the partner token <a href="#id-1-copy-the-affiliates-token" id="id-1-copy-the-affiliates-token"></a>

Login to your [Rewardful dashboard](https://app.getrewardful.com/affiliates) and find the partner you want to give credit for the customer. When you've found the partner, click the "Links" tab and copy one of their link tokens to your clipboard.

[![](https://downloads.intercomcdn.com/i/o/72342080/f161395a093bf7269aa291b1/01-get-link.gif)](https://downloads.intercomcdn.com/i/o/72342080/f161395a093bf7269aa291b1/01-get-link.gif)

### 2. Add the partner token to the Stripe customer <a href="#id-2-add-the-affiliate-token-to-the-stripe-customer" id="id-2-add-the-affiliate-token-to-the-stripe-customer"></a>

Login to your Stripe dashboard and go to the Customers section and find the customer you want to attribute to the partner. Scroll down to the "Metadata" section and click the "Edit" button. Select in the preset list the choice `referral` in the box on the left, and paste the partner token from Step #1 into the right box. Then hit the "Save" button.

[![](https://downloads.intercomcdn.com/i/o/72343190/fa4690d971c6b00ae86963a2/02-stripe-metadata.gif)](https://downloads.intercomcdn.com/i/o/72343190/fa4690d971c6b00ae86963a2/02-stripe-metadata.gif)

### 3. Generate the commission <a href="#h_0cceadd3a1" id="h_0cceadd3a1"></a>

After adding the token of the partner to the customer, refresh the page and in the Metadata section, click on the link under _View in Rewardful_.

Once in the Rewardful page, click the _Generate commission_ button to manually generate the commission.

[![](https://downloads.intercomcdn.com/i/o/345122772/f956fb37014b86750b38f125/2021-06-03\_02-41-45.png)](https://downloads.intercomcdn.com/i/o/345122772/f956fb37014b86750b38f125/2021-06-03\_02-41-45.png)

## Generating commissions for past invoices <a href="#generating-commissions-for-past-invoices" id="generating-commissions-for-past-invoices"></a>

Rewardful will automatically generate commissions for future invoices, but you may want to generate commissions from invoices already paid by the customer (i.e. invoices they paid _before_ you manually associated them with an affiliate).

It's easy to have Rewardful generate commissions for past invoices:

1. Click on the customer's name in Rewardful to view details.
2. Click the "Generate commission" button for the invoices you wish.
3. The page will refresh and show you the newly created commission.

[![](https://downloads.intercomcdn.com/i/o/72343667/ec4bdb1afa825415625bec7a/image.png)](https://downloads.intercomcdn.com/i/o/72343667/ec4bdb1afa825415625bec7a/image.png)[![](https://downloads.intercomcdn.com/i/o/72343743/9ccb28ef35330c49effca3e3/image.png)](https://downloads.intercomcdn.com/i/o/72343743/9ccb28ef35330c49effca3e3/image.png)[![](https://downloads.intercomcdn.com/i/o/72343836/5ea5969851588be4914a2460/image.png)](https://downloads.intercomcdn.com/i/o/72343836/5ea5969851588be4914a2460/image.png)

_Note: If you are also implementing a_ [_double-sided incentive_](https://help.getrewardful.com/en/articles/3024552-double-sided-incentives)_, manually add the coupon code to the customer's Stripe record._
