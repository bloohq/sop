# Competitor Benchmarking

This is where we keep track of various competitors.  We do not want to become obsessed with competitors, but rather key our obsession with our customers.&#x20;

Jeff Bezos puts it well:

{% embed url="https://www.youtube.com/watch?v=aQzuUW3MTio" %}

## Pricing Comparison

We are comparing Blue with our competitors' highest and lowest publicly available monthly prices. Notably, most competitors do not publicly show their prices for Enterprise plans.  We have linked directly to the pricing pages of all competitors to make it easy to review the details.&#x20;



| Platform                                                            | Lowest Monthly Price | Highest Monthly Price |
| ------------------------------------------------------------------- | -------------------- | --------------------- |
| [**Blue**](https://www.blue.cc/pricing)                             | **$7/user**          | **$7/user**           |
| [Monday](https://monday.com/pricing)                                | $12/user             | $24/user              |
| [Asana](https://asana.com/pricing)                                  | $13.49/user          | $30.49/user           |
| [Trello](https://trello.com/pricing)                                | $6/user              | $17.50/user           |
| [Airtable](https://airtable.com/pricing)                            | $24/user             | $54/user              |
| [Wrike](https://www.wrike.com/price/)                               | $9.80/month          | $24.80/user           |
| [Notion](https://www.notion.so/pricing)                             | $10/month            | $18/user              |
| [Basecamp](https://basecamp.com/pricing)                            | $15/user             | $349/organisation     |
| [ClickUp](https://clickup.com/pricing)                              | $10/user             | $19/user              |
| [Jira](https://www.atlassian.com/software/jira/pricing)             | $8.16/user           | $16/user              |
| [Linear](https://linear.app/pricing)                                | $10/user             | $14/user              |
| [Pipedrive](https://www.pipedrive.com/en/pricing)                   | $15/user             | $99/user              |
| [nTask](https://www.ntaskmanager.com/pricing/)                      | $4/user              | $12/user              |
| [Nifty](https://niftypm.com/pricing)                                | $4.90/user           | $499/organisation     |
| [Lark](https://www.larksuite.com/en\_us/plans?)                     | $12/user             | $12/user              |
| [Proofhub](https://www.proofhub.com/pricing)                        | $50/organisation     | $99/organisation      |
| [Taskade](https://www.taskade.com/pricing)                          | $12/user             | $20/user              |
| [Salesforce](https://www.salesforce.com/editions-pricing/overview/) | $25/user             | $165/user             |
