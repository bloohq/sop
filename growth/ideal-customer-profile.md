# Ideal Customer Profile

## Introduction

An Ideal Customer Profile (ICP) is a detailed description of our perfect customer. An ICP outlines the key demographics, behaviours, values, and goals of the ideal customer for which the Blue platform is a perfect fit.  Defining an ICP helps us focus our efforts on the customers we can serve best.

It's important to note that an ICP describes both our buyer — a specific person in an organization — and the organization itself.  This is important because, for ideal results, we need to speak to the right person in the right company at the right time. There's no point in having the perfect company — but then we speak to an intern without buying power or authority.&#x20;

Our ideal customers need solutions that act as [painkillers](sales-funnel/discovery-calls.md), not vitamins or candy.

"_We divide business plans into three categories: candy, vitamins, and painkillers. We throw away the candy. We look at vitamins. We really like painkillers. We especially like addictive painkillers!" —_ [_Kevin Fong_](https://www.linkedin.com/in/kevin-fong-8b2a/)

To clarify, this&#x20;

* **Painkiller —** Solutions that address an organisation's most urgent and intense problems. These are desperately needed, and they were needed yesterday.&#x20;
* **Vitamins —** Beneficial features that are nice-to-have but not essential. While they can add long-term value, vitamins don't solve critical urgent needs directly.
* **Candy —** Fun or delightful features that keep customers engaged but aren't critical to solving core problems. These can help differentiate our platform, but will not sway buying decisions.&#x20;

## Common Characteristics

### Individual

Our ICP for an individual is someone with authority and buying power, even if they are not the only or ultimate decision maker. They are typically the Founder/CEO if the organisation is small or the Project Manager or Head of Operations if the organisation is on the larger side. Other common roles include Head of Marketing and COO (Chief Operating Officer).

Our primary goal is to turn these individuals into internal champions.&#x20;

{% hint style="info" %}
**What is an Internal Champion?**\
An internal champion is someone in a potential client's organization who supports and promotes Blue. They understand our value proposition and encourage their company's decision-makers to choose us. This person is crucial in convincing their organization to buy Blue, helping to overcome doubts and ensuring a smooth introduction to our platform.
{% endhint %}

They want a solution to make their life easier and solve their pain. If they are not the founder/CEO, they will also want something that will make them look good — they found a cost-effective solution that works well!&#x20;

These individuals also understand that there are two key aspects to a successful implementation of a collaboration and process management platform:

1. The platform — The features and functionalities and the work that they enable.&#x20;
2. The processes — _How_ the work is done, and this implementation is a good opportunity to update old processes.&#x20;

### Company

Our ICP at the company level is a company that is beyond  15 to 20 full-time staff. This is important because, at this stage, all the freemium accounts they have across their entire SaaS Technology stack start to charge them simultaneously. They may have gone from a few hundred dollars a month in SaaS spending to suddenly having several thousand in SaaS spend per month.&#x20;

This can also be a particular division within a larger organization of 1,000 to 5,000.

{% hint style="danger" %}
**Caution: Engaging with Very Large Organizations**

Our focus should be on organizations we can serve effectively. Engaging with very large entities (over 5,000 employees) seeking a system-wide solution can lead to extensive and complex interactions with their IT departments, multiple reviews, and numerous stakeholders. These processes often involve a substantial time investment with a relatively low probability of success. It's advisable to target organizations where we can provide optimal value and where the decision-making process aligns better with our capabilities.
{% endhint %}

This is a good opportunity for Blue because we come in with a [cost-effective offer](competitor-benchmarking.md) that helps to alleviate the pain point of cost, especially when we compare our feature set to the enterprise feature set of other platforms.&#x20;

Ideally, the customer does not have a project or process management platform. They have been using Excel or manual workflows and are quickly outgrowing them. They are spending a significant amount of time doing double data entry and other manual work, so an investment in the project and process platform will have a clear ROI (Return on Investment)

Common industries include:

* Professional Services&#x20;
* Marketing
* Design Companies
* Technology Companies
* Lawyers
* Construction Firms
* Local Government

Common use cases include:&#x20;

## Pain Points

These pain points are typically experienced by customers who buy from Blue. If a prospect has three or more of these pain points, they will likely be a very good fit.&#x20;

1. **No Single Source of Truth**: Struggling with multiple, disjointed information sources.
2. **Over-reliance on Excel**: Using Excel for complex project and workflow management.
3. **Lack of Standardized Processes**: Absence of uniform procedures across teams and projects.
4. **Manual and Inefficient Workflows**: Heavy reliance on manual methods leading to inefficiencies.
5. **Complexity of Previous Tools**: Previous tools were too complex, leading to low adoption by teams.
6. **Budget Constraints**: Limited financial resources for investing in large-scale, expensive solutions.
7. **Inadequate Reporting**: Difficulty in generating meaningful, actionable reports from existing systems, leading to poor decision-making.

We can map the common points to our painkillers.&#x20;

<table><thead><tr><th>Pain Point</th><th>Painkiller</th><th data-hidden></th></tr></thead><tbody><tr><td><strong>No Single Source of Truth</strong></td><td>Blue has a full audit trail, and we can cross-reference data.</td><td></td></tr><tr><td><strong>Over-reliance on Excel</strong></td><td>Import Excel files in 5 minutes and have your custom data structure.</td><td></td></tr><tr><td><strong>Lack of Standardized Processes</strong></td><td>Blue forces a defined structured workflow, which can be edited when required..</td><td></td></tr><tr><td><strong>Manual and Inefficient Workflows</strong></td><td>Automations</td><td></td></tr><tr><td><strong>Complexity of Previous Tools</strong></td><td>Our slogan is: Teamwork, Made Simple! </td><td></td></tr><tr><td><strong>Budget Constraints</strong></td><td>Blue is <a href="competitor-benchmarking.md">affordable</a>: $7/month/user</td><td></td></tr><tr><td><strong>Inadequate Reporting</strong></td><td>Cross-Project Dashboards with stat cards, pie charts, bar charts and filters. </td><td></td></tr></tbody></table>

## Buying Behaviour

The individual we are dealing with will typically evaluate another 2-3 systems, and we must understand which systems these are so we can best position Blue in the most positive light. The individual should also be inviting other team members to calls and demos, so we can build consensus with the entire team.&#x20;

It is a hugely positive sign if the individual signs up to a free trial and sends our Customer Success team data to import or has a free support call to get onboarded.&#x20;

* Will be typically evaluating 2-3 other systems.&#x20;
* Should be signing up to a free trial&#x20;

## Red Flags 🚩

* Slow to reply
* No-show without an apology after follow-up
* Requires complex custom integrations&#x20;
* Industries to avoid:
  * Banking — Has significant security requirements and often want on-premise solutions.
  * Healthcare — Has specific security requirements that we do not fulfil yet.
  * Central Government — Too slow, and extremely unlikely to use a SaaS solution.



## Sample Customer Persona

**Name:** Alex Johnson&#x20;

**Role:** Project Manager&#x20;

**Company:** A mid-sized technology firm with 20-30 employees based in the USA.

**Needs:** Alex is looking for a cost-effective solution to streamline their current project and process management, which is currently reliant on Excel and manual workflows.&#x20;

**Pain Points:** The company struggles with no single source of truth, over-reliance on Excel, lack of standardized processes, manual and inefficient workflows, and budget constraints.&#x20;

**Behaviour:** Alex is evaluating 2-3 other systems and is keen on a free trial. He values simplicity and efficiency and seeks a platform that offers robust reporting tools. . He shows proactive engagement by actively participating in product demos and asking insightful questions. His feedback is generally positive, particularly appreciating features like the easy import of Excel files, automations, and structured workflows. Moreover, Alex indicates the potential for referrals to other departments or partner companies if his experience with the platform meets his expectations. This combination of engagement, feedback, and referral inclination presents a promising outlook for his relationship with the platform.

**Why Blue:** Alex finds Blue's platform attractive due to its affordability, ability to import Excel files easily, structured workflow, and comprehensive reporting capabilities.

