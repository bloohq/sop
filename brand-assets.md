# Brand Assets

Here are our brand assets available for download.

You can download all the logos in this zip file:

{% file src=".gitbook/assets/Blue Logos.zip" %}

## Logo PNGs

<div>

<figure><img src=".gitbook/assets/Blue logo (black) (1).png" alt=""><figcaption><p>Black PNG</p></figcaption></figure>

 

<figure><img src=".gitbook/assets/Blue logo (white).png" alt=""><figcaption><p>White PNG</p></figcaption></figure>

 

<figure><img src=".gitbook/assets/Blue logo (1).png" alt=""><figcaption><p>Blue PNG</p></figcaption></figure>

</div>



## Logo SVGs

<div>

<figure><img src=".gitbook/assets/Blue logo (black).svg" alt=""><figcaption><p>Black SVG</p></figcaption></figure>

 

<figure><img src=".gitbook/assets/Blue logo (white).svg" alt=""><figcaption><p>White SVG</p></figcaption></figure>

 

<figure><img src=".gitbook/assets/Blue logo.svg" alt=""><figcaption><p>Blue SVG</p></figcaption></figure>

</div>



## Application Icons

<div>

<figure><img src=".gitbook/assets/Blue app icon 1 (2).png" alt=""><figcaption><p>Blue on White</p></figcaption></figure>

 

<figure><img src=".gitbook/assets/Blue app icon 2 (1).png" alt=""><figcaption><p>White on Blue</p></figcaption></figure>

</div>

## Backgrounds

There are six backgrounds of different Blue shades and gradients for product screenshots.&#x20;

{% file src=".gitbook/assets/bluebackgrounds.zip" %}

<div data-full-width="true">

<figure><img src=".gitbook/assets/background6.svg" alt=""><figcaption></figcaption></figure>

 

<figure><img src=".gitbook/assets/background5.svg" alt=""><figcaption></figcaption></figure>

 

<figure><img src=".gitbook/assets/background4.svg" alt=""><figcaption></figcaption></figure>

 

<figure><img src=".gitbook/assets/background3.svg" alt=""><figcaption></figcaption></figure>

 

<figure><img src=".gitbook/assets/background1.svg" alt=""><figcaption></figcaption></figure>

 

<figure><img src=".gitbook/assets/background2.svg" alt=""><figcaption></figcaption></figure>

</div>

## Corporate Font

Our corporate font is **Inter.**

You can download this here:

{% file src=".gitbook/assets/Inter.zip" %}
The full Inter Font Family
{% endfile %}

You can also find Inter on Google Fonts:

{% embed url="https://fonts.google.com/specimen/Inter" %}
Inter is a Google Font
{% endembed %}
