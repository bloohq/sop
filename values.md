---
description: Nothing meaningful was ever accomplished in the short term.
---

# Values

These values are our directional signposts. They are not dogma, but they guide us in our day-to-day decision-making. If we break them from time to time, that's fine, as long as it's a deliberate choice with a rational foundation.&#x20;

1. **Simplicity —** Anyone can make things complex. We strive to make things as simple as they should be, but no simpler.
2. **Long-Term Thinking —** We make decisions based on the long term — even if that means hard work or difficult choices.
3. **Clarity —** We prioritize ideas that are easy to understand and convey to anyone.
4. **Attention to Detail —** The small things _are_ the big things. We take pride in our attention to detail.
5. **Customer Obsession —**We don't have investors, so we must have an incredible focus on our customers.
6. **Deep Work —** We value collaboration, but we know that quality requires time working alone, deeply.
