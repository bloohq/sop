# Extending Free Trials

Blue offers a 7-day free trial for all new organisations. As the trial nears expiration, emails are automatically sent to remind the customer.

If more time is required to try Blue or conduct testing, organszation admins can request an extended free trial. Below are the step-by-step instructions for the Blue team to process these extension requests.

### Overview

* Free trials can be extended by 1-2 weeks depending on the customer's needs
* Only the Blue team can process extensions through the admin section
* The organisation administrator must first request the extension
* Multiple extensions can be provided if needed for extensive testing

### Process

1. Open the Blue Admin section
2. Click on the organization requesting an extended trial
3. Click the "Extend Trial" button
4. Select the extension duration (typically 1-2 weeks)
5. Click Update to confirm the new expiration date

<figure><img src="../.gitbook/assets/CleanShot 2024-01-26 at 10.40.56@2x.png" alt=""><figcaption></figcaption></figure>

The free trial will now be automatically extended for the selected duration.





