---
description: >-
  This is a useful cheat sheet, and also a good context-starter for LLM (Large
  Language Model) prompts when writing documentation, marketing copy, and
  roadmap planning.
---

# Feature List

\


* **General:**
  * Blue is a powerful project and process management platform with 6,000+ customers across 120+ countries. Has been operating for over six years (since 2018).
  * Native iOS and Android apps are available.
  * Blue is a Progressive Web App (PWA) with MacOS and Windows desktop apps and a web interface.
  * Support Dark Mode
  * The interface is real-time, with instant updates without requiring a page refresh.
  * Blue offers unlimited storage, 20+ languages, and a free 7-day trial without a credit card.
  * User Benefit: Provides a comprehensive, all-in-one solution for project and process management, streamlining workflow across various devices and platforms.
* **Pricing:**&#x20;
  * $7/user/month or $70/user/month (2 months free).&#x20;
  * Paid Seats: Project Admin, Team Members, Custom User Roles
  * Free Seats: Clients, Comment-Only, View-Only
  * NGOs/Non-Profits get 50% off
  * User Benefit: Offers flexible and affordable pricing options that cater to the needs of different organizations, ensuring value for money and scalability.
* **Key Value Propositions:**
  * Simplicity (leading to Faster Onboarding, Less Training Overhead, Higher Chance of Adoption)
  * Affordability (leading to Less Cost, No Penalty for Growth, Only Pay for Core Team)
  * Flexibility & Customisation (leading to Differentiation, Faster & Easier Onboarding, Flexible Use Cases)
* **Support:**
  * Email: help@blue.cc
  * Documentation: https://documentation.blue.cc
  * 30-minute free support call: https://app.onecal.io/b/blue/support
  * Community Forums: https://ask.blue.cc
  * Paid support for data import, API integrations, custom features, and staff training.
  * User Benefit: Guarantees peace of mind with multiple support avenues, ensuring users can easily find help and resources when they need them.
* **Projects:**&#x20;
  * Framework for organizing users and data.&#x20;
  * Represent initiatives or processes.&#x20;
  * Project Navigation Bar has: Activity, Records, Chat, Wiki, Docs, Files, Forms, People
  * Convertible to templates.&#x20;
  * Copyable, archivable, deletable.
  * Admin can set project description, icon, color, and rename "records" to something else such as "tasks, "leads", "tickets".&#x20;
  * Users can organise projects into color-coded folders on the sidebar
  * User Benefit: Simplifies project organization and management, allowing teams to easily structure work, track progress, and adapt to changes.
  * Upload PDF: This will allow you to generate a printable/downloadable PDF template of your records. You can use it for invoicing, storing data that is in a portable document format.&#x20;
* **Records:**
  * Building blocks for projects.&#x20;
  * Highly customizable and can represent tasks, sales, candidates, etc.
  * Default Fields: Name, dates, assignees, tags, dependencies (blocking or blocked by another record), rich-text description
  * Records can have checklists with checklist items, that can have due dates and assignees
  * Records have comments that support rich text, attachments, embeds, @mentions, GIFs.
  * Can set recurring records based on custom-defined schedules and decide which list to create repeated records in and what default fields to copy across into the repeated record.&#x20;
  * Users can set custom reminders via email or push notifications&#x20;
  * User Benefit: Enables detailed and flexible tracking of all project elements, ensuring tasks, sales, candidates, etc., are efficiently managed and monitored.
* **Views:**&#x20;
  * User Benefit: Offers multiple perspectives on project data, facilitating better planning, tracking, and management of tasks and timelines. All basic and advanced filters work with all views, enabling users to focus on what's important.&#x20;
  * Kanban Boards: Visually map out workflow with customizable lists and drag-and-drop functionality.
  * Calendars: Visualize records in a calendar format with due dates, assignees, and filters.
  * Gantt Charts: Track project timelines, dependencies, and progress with interactive Gantt charts. Choose between zoom levels: Year, quarter, month, week, or day. Organize timeline by list, assignee (great for team resource management!), or tag.&#x20;
  * Databases: Manage large datasets with a spreadsheet-style view, grouping, and bulk actions.
  * Maps: Plot location-based records on an interactive map with color-coded pins and filters.
  * Lists: Streamlined checklist view for managing records in a linear format with sorting and filtering.
* **Filters**
  * Ability to filter by tag, assignees, due dates, and completed/non-completed records. This allow you way you can see a clearer view of your board.&#x20;
  * Filter by Custom Fields: In case you want to look for a specific email address under custom fields or using different condition such as empty/not empty, you can use advanced filter to narrow down your records.&#x20;
* **Custom Fields:**
  * Ability to define custom data structure in projects. Each custom field is then available in all records within that project
  * User Benefit: Allows for the creation of a tailored project management environment that matches the unique needs of the team, enhancing data organization and accessibility.
  * Single Line Text: Captures unformatted single-line text strings of any length.
  * Multiple Line Text: Allows entry of longer, multi-paragraph formatted text content.
  * URL/Link: Stores and validates web addresses and links with click-to-open functionality.
  * Currency: Handles monetary values and amounts with support for multiple currencies and localized formatting.
  * Country: Provides a searchable dropdown list of all recognized countries with flag icons.
  * Date: Enables selection of dates, times, and ranges with an interactive date picker.
  * Single Select: Creates a dropdown list with a single selectable option from predefined choices.
  * Multiple Select: Allows multiple selections from a customizable dropdown list of options.
  * Location/Map: Captures geographic coordinates or search for addresses and displays locations on an interactive map.
  * Phone Number: Stores and validates phone numbers with country-specific formatting and click-to-call, opening default call software.&#x20;
  * Email: Captures and validates email addresses with click-to-email, opening default email client.
  * Star Rating: Enables rating and scoring using a customizable star-based system.
  * Checkbox: Provides a simple on/off or true/false checkbox toggle.
  * Number: Captures numeric values within a specified range with validation.
  * Percent: Handles percentage values with validation and customizable precision.
  * Formula: Defines calculations based on values from other fields using functions and operators. Updates in real-time and can be used in dashboards.
  * Unique ID: Automatically generates a unique alphanumeric identifier for each record. Support a custom Prefix.
  * Reference: Establishes a link to records in other projects for cross-project data connectivity.
  * Lookup: Retrieves and displays data from referenced records in other projects.
  * Duration: Calculates the time elapsed between two specified actions on the record. Good for measuring SLAs.
  * File: Allows uploading and attaching files of various types to records.
* **Automations:**&#x20;
  * Powerful if-this-then-that workflows to automate processes.&#x20;
  * Supports multiple triggers and actions like updating fields or sending emails.
  * User Benefit: Reduces manual effort and increases efficiency by automating routine tasks and workflows, allowing teams to focus on more strategic work.
* **User Management:**
  * Each project is private, only users specifically invited can see the data.&#x20;
  * Roles: project admin, team member, client, comment-only and view-only roles.&#x20;
  * User Benefit: Ensures secure and controlled access to project information, allowing for precise role-based permissions and collaboration among team members.
  * There are also custom user roles that define granular access:&#x20;
    * User role Name&#x20;
    * User role description&#x20;
    * View project activity (on/off)&#x20;
    * View project forms (on/off)&#x20;
    * Assigned Records Only:Allow this role to view only assigned Records inside this project. (on/off)&#x20;
    * Allow Invite Others:Allow this role to others into the project with the same role. (on/off)
    * Custom fields: For each custom field in the project, set view/edit permissions&#x20;
    * List: for each list in the project, set view/edit/delete permissions
* **Dashboards:**&#x20;
  * Unlimited Number of dashboards per project
  * Each dashboard has real-time charts combining data across projects.
  * Dashboards can be copied&#x20;
  * User Benefit: Provides real-time insights into project performance and metrics, enabling informed decision-making and strategic planning.
  * Available Charts:
    * Stat Card
    * Pie Chart&#x20;
    * Bar Chart
* **Wiki & Docs:**&#x20;
  * Tools for collaborative knowledge management.&#x20;
  * User Benefit: Centralizes knowledge management and documentation, fostering team collaboration and ensuring important information is easily accessible.
* **Files**&#x20;
  * Upload files directly to Blue&#x20;
  * You can create folders to organize the file&#x20;
  * Each file can be shared publicly, by right-click and generate a shareable link to let other download. Turn it off when you no longer want other people to access.&#x20;
* **Forms:**&#x20;
  * Customizable intake forms for gathering data from external users into Blue.&#x20;
  * You can drag and drop any of the custom fields available
  * Form can be customized with a name, logo, colour scheme. “Powered by Blue” at the bottom can be turned off
  * Sharing: As a link, QR Code, or via an embed code that can be sent to developers to embed Blue forms into websites
  * Form entries can be automatically saved to a specific list in the project, assigned to a person, and tagged. Form entries can also trigger automations for even more flexibility.&#x20;
  * Option for redirect URL (i.e. to a thank you page) once a user submits a form
  * Forms can have an optional footer to have terms and conditions
  * Forms can be copied across projects
  * User Benefits: Streamlines data collection and integration into projects, making it easier to gather and organize information from external sources.
* **Integrations:**&#x20;
  * GraphQL API: 100% API coverage for all features. Read, Write, and Stream data.&#x20;
  * Webhooks: Trigger webhooks on almost all actions
  * Third-Party: Direct integrations with 5000+ apps via Pabbly Connect and Zapier
  * Calendar: Integrate with Outlook/Gcal/iCal at a company/project/assignee level
  * CSV: import/export for up to 250,000 records
  * User Benefits: Enhances productivity and workflow by seamlessly connecting with a wide range of external apps and services.
* **Notifications:**
  * Main On/Off Notification Switch
  * Users can choose to turn on/off email and desktop/mobile push notifications for each type of notification.
  * There is a notification bell on the top right for @mentions
  * Notifications can be turned On/off per project
  * In projects, admins can create custom email notifications using email automations
  * User Benefits: Ensures that users can stay up-to-date on what's precisely important to them without being overwhelmed with too many notifications.
* **Activity Feed & Audit Logs:**
  * Activity Feeds:
    * Company-wide activity feed across all projects. Users see a custom activity feed based on the projects they are in.&#x20;
    * Project-level activity feed. The same for all users, except Custom User Roles, who may not see certain activity items
    * These support filters by person, tag, and date
  * Record-level audit logs for any changes within the record
  * These cannot be edited, so all actions are fully auditable and logged.
  * User Benefit: Offers transparency and traceability in project activities, ensuring accountability and facilitating project review and analysis.&#x20;
* **User Settings:**
  * First and Last Name
  * Job Title (optional)
  * Birthday (optional)
  * Date Format (dd/mm/yy or mm/dd/yy or yyyy/mm/dd)
  * Email
  * Timezone
  * Profile Photo (optional)
  * Interface color (7 choices)
  * User Benefit: Allows for a personalized user experience, enabling individuals to tailor the platform to their preferences and needs.
* My Records (Your Name tab)&#x20;
  * Any records that has been assigned to you across all project can be seen under My records tab, allowing you to see tasks and checklist items that are overdue, currently, no due dates. You can save time by going through each records here and mark it complete directly.&#x20;

