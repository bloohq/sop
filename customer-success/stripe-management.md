# Stripe Management

Upgrading customers to higher subscription plans or adding additional products and services is an important part of nurturing growth with clients. This document outlines detailed procedures for the most common customer upgrade scenarios our teams handle, to ensure accuracy and consistency across every client interaction.

Following these SOPs as written ensures every customer upgrade is executed smoothly, payments are processed accurately, and new plan permissions are fully enabled. This prevents common accidental downgrades, billing problems from outdated cards, or platform conflicts across tools.&#x20;



## **Upgrading Existing Customers**

1. Access the customer’s profile page in Stripe. Locate the “Customers” section in the left sidebar.
2. Click on the name of their current subscription plan (e.g. “Blue Business”). This will open up the full details of their current subscription.
3. Click on the “Add Product” button. This opens a window to search for and select the new product you want to upgrade them to.
4. Search for and select the upgraded plan to add (e.g. “Blue Enterprise”).
5. If there has been an agreed-upon discount, click into the Discounts & Coupons section and create or apply the relevant coupon code that reflects the decided-upon pricing.
6. Alternatively, we can create an entirely new product in Stripe that accurately displays the custom pricing agreement made with the customer.
7. Click “Update Subscription” after applying the add-on product to initiate the upgrade. This will bill the additional amount owed for the upgraded subscription immediately.

## **Upgrading Appsumo or Blue Tier Customers**

1. In the Blue Admin backend, locate and access the customer’s profile that needs to be upgraded.
2. Click to revoke their current Blue license, which downgrades them to a free account temporarily.
3. Notify the customer via email that you require them to provide new card details.

Some key details to provide in the message:

* As Appsumo users are not listed in Stripe, we need to recapture their updated payment information for the upgraded subscription.
* A link or directions where they can securely submit their new card information.
* Next steps once you receive their card details.

4. After receiving the new card details, the customer should now be fully listed under the customer profiles in Stripe.
5. Follow the same upgrading subscription steps outlined for existing subscribed customers in Stripe (Adding the upgraded Enterprise product, applying discounts/custom pricing, updating the subscription).

## **Linking Separate Invoices via Metadata**

If a Blue user pays their subscription through a separate invoicing method, their Stripe and Blue accounts still need to be connected. This is done by adding their metadata from Metabase.

1. Login to the Metabase data analytics platform.
2. Utilize the search bar to find the customer's entry based on their email address, name, company ID number, etc.
3. Within their profile, scroll to the metadata section to map their associated ID number, Email, Company ID, Company Name, Slug details between their Blue and Stripe accounts.
4. Click save once all metadata has been accurately linked.
5. This will upgrade the account and apply Enterprise permissions based on the outside payment details.



## **Addressing Automatic Downgrades or Account Locks (Rare Instances)**&#x20;

Such issues typically arise due to a prior subscription auto-renewing, leading to unintentional downgrades or account locks.

Steps to address:

* Mark the invoice as uncollectible.
* Discuss payment with the customer. Once we reach an agreement on the payment date, mark the invoice as paid. This will prevent future unintentional account locks or downgrades.
* Head of engineering will manually apply enterprise manually on the database
* Create a new open invoice:
  * Navigate to the customer's profile on Stripe.
  * Select "Create Invoice."
  * Add the necessary product (typically a one-off payment).
  * Send the invoice to the customer, ensuring it remains in an open status until payment is received.

## **White Label & Custom Domains**

### **For Users with Card Details on Stripe:**

* Navigate to the customer's profile on Stripe.
* Click “Action” and create Custom Domain subscription.
* Charge directly from the customer's registered card.

### **For Users Without Card Details:**

* Navigate to the product page.
* Select the 'Custom Domain' option.
* Discuss the subscription duration with the customer: either monthly or yearly.
* Upon reaching an agreement, copy the payment link and send to the customer.

### **Post-Payment:**

* Once the customer has completed the payment, set up the custom domain on Render.
* Forward the DNS configuration instructions to the customer.
* Activate the custom domain for the customer's use.

## Remove and Apply ACH Credit Legacy

When a customer ask for a bank transfer instead of card payment

* Go to customer profile then click edit on the current subscription that they have
* Go all the way down to Payment Method option
* Click “Manage Payment”
* Remove CashApp or Credit Debit
* Only select “ACH Credit Transfer”
* Click update subsciprition and once the charges start the customer should be able to receive an invoice.

## Changing price and upgrade to enterpise

When a customer had previously charged after trial and want to change price.

* Go to customer profile, then click edit on the current subscription that they have
* Remove current pricing and add new pricing
* Proprate the changes (in case you refunded)
* Select for customer to manually pay, select debit or credit
* Save changes

On the invoice

* Go to Draft
* Then manually send them the invoice, so it will get sent right away.

## Adding/Removing Users&#x20;

For Per User pricing organizations&#x20;

**To Add more Users**&#x20;

* Navigate to the customer profile.
* Click on "Edit" for the current subscription.
* Adjust the quantity (QTY) by adding the desired number of users (e.g., if adding 2 more users, increase the quantity by 2).
* Click "Update the subscription."
* An invoice will automatically generate and be sent to them for the additional users.

**To Remove Users**

When the user ask to remove a user from their organization&#x20;

* Click on "Edit" for the current subscription.
* Adjust the quantity (QTY) by removing the specified number of users (e.g., if removing 2 users, decrease the quantity by 2).
* Click "Update the subscription."
* Go to the ongoing invoice and mark it as 'Void.'

