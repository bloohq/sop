# Writing Documentation

## Extracting Documentation

Extracting the entire documentation to a MarkDown file is useful if you want to use AI LLMs (Large Language Models) such as [OpenAI's ChatGPT](https://openai.com/chatgpt) or [Anthropic's Claude ](https://claude.ai)to help you write new documentation pages, as the generated MarkDown file can be used to provide the AI with context.

To extract documentation from our [public doc repo](https://gitlab.com/bloohq/documentation), run this Python script:

```python
import os
import git
import glob
import shutil  # Import shutil for removing directories

# Function to get the directory of the current script
def get_script_dir():
    return os.path.dirname(os.path.realpath(__file__))

# Clone the repository
repo_url = 'https://gitlab.com/bloohq/documentation'
script_dir = get_script_dir()  # Get the directory of the script
repo_dir = os.path.join(script_dir, 'documentation')  # Directory to clone the repo

# Check if the script_dir is writable
if not os.access(script_dir, os.W_OK):
    print(f"The directory {script_dir} is not writable. Please check your permissions.")
else:
    if not os.path.exists(repo_dir):
        git.Repo.clone_from(repo_url, repo_dir)

    # Navigate through the cloned repository and find all .md files
    md_files = glob.glob(repo_dir + '/**/*.md', recursive=True)

    # Concatenate the contents of all .md files into one
    combined_md_content = ''
    for md_file in md_files:
        with open(md_file, 'r', encoding='utf-8') as file:
            combined_md_content += file.read() + '\n\n'

    # Save the combined content into one .md file
    combined_md_filename = os.path.join(script_dir, 'blue_docs.md')
    with open(combined_md_filename, 'w', encoding='utf-8') as combined_file:
        combined_file.write(combined_md_content)

    print(f'All Markdown files have been combined into {combined_md_filename}')

    # Delete the cloned directory and its contents
    shutil.rmtree(repo_dir)
    print(f'The directory {repo_dir} and its contents have been deleted.')

```

