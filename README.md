# Introduction

Yes, these are Blue's SOPs (Standard Operating Procedures) — shared publicly for the world to read.&#x20;

We do this for multiple reasons:

1. **Trust —** We know that by using Blue, customers are putting considerable trust in us. They upload sensitive organisation information and use us for mission-critical processes and workflows.  So it is their right to know we are running our organisation well.
2. **Inspiration —** By reading our SOPs, we hope this inspires you to write your own. We believe that everything is a process that starts when an individual or team thinks about it — and the best way to think is to write.&#x20;
3. **Scalability —** [Startups = Growth](https://www.paulgraham.com/growth.html), so we plan to keep growing sustainably. This means that anything that we do more than once has to be written down so that key information is not locked in someone's head.&#x20;

[You can download the source content of this SOP and see all historical updates here.](https://gitlab.com/bloohq/sop)

Processes are similar to having a life philosophy — it is _not_ optional. If you don't have a life philosophy, then your life philosophy is _not_ to have a coherent life philosophy. If you don't have written processes, you still have ways of doing things.&#x20;

And so, having SOPs means having a theory on why we do things the way we do things:&#x20;

{% embed url="https://www.youtube.com/watch?v=Lj9OTFsO8e4" %}

Writing things down provides an opportunity to always question what we do. We can critically examine our processes and use Reason to review, improve, or cut processes when needed.&#x20;

## Simple, Logical, Repetable.

A key idea of having an SOP is to keep our processes simple, logical, and repeatable. This ensures we don’t have long-term dependencies on any key individual but work as a team.

The SOPs are _not_ a replacement for common sense.

The best thing to do in any situation is whatever you believe is in customers' best interest, regardless of what is written here. Reality is far too complex to be effectively represented in a text document. So, the purpose here is not to tell you what to do in any given situation but to give you a starting point so we don’t have to reinvent the wheel whenever a situation arises.

_“If you can’t describe what you are doing as a process, you don’t know what you’re doing”_ \
**W. Edwards Deming, **_**Total Quality Management.**_&#x20;



