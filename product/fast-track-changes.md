# Fast-Track Changes

For smaller changes that do not require the full release process, we add the tag "fast-track".&#x20;

Examples of Fast-Track Changes:

* **Fixing Typos**: Correcting spelling mistakes or grammatical errors in the user interface.
* **Small UI Adjustments**: Minor modifications to the layout or design that do not significantly affect the overall functionality or user experience, such as adjusting the padding around a button.
* **Adding Icons**: Introducing new icons for better visual guidance or to enhance the user interface's aesthetic appeal without altering the underlying functionality.

This means the feature or change can go out to customers within 24-48 hours.

The updated process for changes that are tagged fast-track:

1. Begin by creating a new branch from the main branch. This ensures your changes are based on the most recent and stable codebase version. Implement the Changes:
2. Make the necessary adjustments or additions as per the fast-track criteria. Ensure that your changes are precise and limited to the scope of a fast-track modification. Create a Merge Request for Each Target Branch:
3. Once your changes are ready, initiate a merge request for each branch where the changes need to be applied (Dev, Beta, and Production). It's crucial to cherry-pick only the specific changes you've made to avoid unintended modifications being included. Review and Generate a Preview Link:
4. The team will review your merge request. During this phase, a preview link is automatically generated, allowing reviewers to inspect and test the changes in a controlled environment visually. This step ensures that the modifications meet the quality standards and function as expected.
5. &#x20;Following approval, your changes are merged into the Development (Dev), Beta, and Production branches simultaneously.&#x20;

#### Key Points to Remember

* **Precision**: Ensure that the changes are limited to what's necessary for the fast-track process. This minimizes the risk of errors or unintended consequences.
* **Documentation**: Even though these are minor changes, documenting what was changed, why, and how it was tested is crucial for transparency and future reference.
* **Communication**: Keep relevant team members informed about the fast-track changes, especially those who manage or monitor the Dev, Beta, and Production environments. This helps coordinate the release and address any issues promptly.
