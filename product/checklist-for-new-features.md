# Checklist for New Features

This is our checklist of considerations when designing and building new features. This is to help us remember what implications there may be for a new feature and can guide us on the scoping of the new feature when writing a brief.&#x20;

* [ ] API
* [ ] Webhooks
* [ ] Custom Permissions
* [ ] Import
* [ ] Export
* [ ] Automations
* [ ] Email Automation {tags}
* [ ] Mobile Responsive View
* [ ] Mobile Applications&#x20;
* [ ] iPad Application
* [ ] Notifications
* [ ] Wiki&#x20;
* [ ] Chat
* [ ] Forms&#x20;
* [ ] People&#x20;
* [ ] File&#x20;
* [ ] Search
* [ ] @mention&#x20;
* [ ] Calendar
* [ ] Templates
* [ ] Archive Project&#x20;
* [ ] Copy / Move Project
* [ ] Copy / Move Record
* [ ] Repeating&#x20;
* [ ] Custom Field&#x20;
* [ ] Written Documentation
* [ ] Helpers

