# Environments

Blue has three main environments:

## 1. Development

[The Blue Development Environment](https://dev.app.blue.cc) is the bleeding edge, often with breaking changes and has a unique database instance. This contains all the latest in-progress features. Used by Blue team to test for changes, and by customers who are interested in trying things out.&#x20;

{% hint style="danger" %}
**While we encourage customers to try our Blue Development Environment to see new features, make sure that you do not use it for real work, as we regularly refresh and wipe the data on this environment.**&#x20;
{% endhint %}

## 2. Beta

[Blue Beta ](https://beta.app.blue.cc/)is our public Beta program.  It has features that are currently in Beta testing, but it shares the database with the production environment. It has a fraction of the infrastructure resources of the production environment. This means that any customer can log in and use Beta. The data that they have in the production environment will be shown, and any changes they make will automatically sync in the production environment.&#x20;

Features typically live in Beta for 4-6 weeks before being released into production. This ensures that we have meaningful statistics on stability and usage. This is important because, in our opinion, stability is itself a feature.

## 3. Production

The [Blue Production Environment](https://app.blue.cc/) is our main version of Blue, where we optimize for stability and uptime.&#x20;
