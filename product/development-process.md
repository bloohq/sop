# Development Process

At Blue, we have a dedicated project for our product development that is called "Product".

This is where we track the entire process of developing a new feature, from ideation to specification to development, testing, and release.

## Main Process

The project has the following lists:

* **Ideas/Feedback —** This is a list of team ideas or customer feedback based on calls or email exchanges. Feel free to add any ideas here! In this list, we have not decided that we will yet build any of these features, but we regularly review this for ideas that we want to explore further
* **Backlog (Long Term) —** This is where features from the Ideas/Feedback list go if we decide they would be a good addition to Blue.
* **{Current Quarter} —** This is typically structured as "Qx YYYY" and shows our quarter priorities.&#x20;
* **Bugs —** This is a list of known bugs reported by the team or customers. Bugs added here will automatically have the "Bug" tag added.
* **Specifications —** These features are currently being specified. Not every feature requires a specification or design; it depends on the expected size of the feature and the confidence level that we have with regards to edge cases and complexity.  As the team at [Mäd](https://mad.co) provides the design, they are in the project witha custom role that only gives them access to Ideas/Feedback, Bugs, Specifications, Design Backlog, In Progress Design, Design Review.
* **Design Backlog —** This is the backlog for the designers, any time they have finished something that is in progress they can pick any item from this list.&#x20;
* I**n Progress Design — T**his is the current features that the designers are designing.
* **Design Review —** This is where the features whose. designs are currently being reviewed.&#x20;
* **Backlog (Short Term) —** This is a list of features we will likely start working on in the next few weeks. This is where assignments take place. The CEO and Head of Engineering decide which features are assigned to which engineer based on previous experience and workload.&#x20;
* **In Progress —** These are features that are currently being developed.
* **Code Review —** Once a feature has finished development, it undergoes a code review. Then it will either be moved back to "In Progress" if adjustments are needed or deployed to the Development environment.
* **Dev —** These are all the features currently in the [Development environment.](https://dev.app.blue.cc)  Other team members and certain customers can review these.&#x20;
* **Beta —** These are all the features currently in the [Beta environment. ](https://beta.app.blue.cc)Many customers use this as their daily Blue platform and will also provide feedback.&#x20;
* **Production —** When a feature reaches production, it is then considered done.&#x20;

{% hint style="info" %}
**A note on Feature Versions**

Sometimes, as we develop a feature, we realize that certain subfeatures are more difficult to implement than initially expected, and we may choose not to do them in the initial version that we deploy to customers. In this case, we can spin up a new record with a name following the format "{FeatureName} V2" and include all the subfeatures as checklist items.
{% endhint %}

## Tags

These are used to identify the type of feature in our process quickly. One feature may have multiple tags.&#x20;

* **Mobile —** This means that the feature is specific to either our iOS, Android, or iPad apps.&#x20;
* {**EnterpriseCustomerName} —** A feature is specifically being built for an enterprise customer.  Tracking is important as there are typically additional commercial agreements for each feature.&#x20;
* **Bug —** This means that this is a bug that requires fixing.&#x20;
* **Fast-Track —** This means that this is a [Fast-Track Change](fast-track-changes.md) that does not have to go through the full release cycle as described above.&#x20;
* **Main —** This is a major feature development. It is typically reserved for major infrastructure work, big dependency upgrades, and significant new modules within Blue.&#x20;
* **AI —** This feature contains an artificial intelligence component.&#x20;
* **Security —** This means a security implication must be reviewed or a patch is required.&#x20;

## Custom Fields

We have the following custom fields:

* **Specification—** This link to a Google Doc or Blue Doc [with the detailed specification.](../customer-success/writing-documentation.md)
* **MR —** This is a link to the Merge Request in GitLab. All features must have this once they are in Code Review.&#x20;
* **Preview Link —** If the feature is just a front-end change, Render will generate an automatic preview link once you commit your code. Paste it here so others can easily see your changes on a fully working front-end.&#x20;
* **Lead —** Which senior engineer is in charge of the code review for this feature.&#x20;

## Checklists

We use checklists to drop real-time feedback during our weekly demo sessions, and it is a good way to track what you need to do to complete a feature.&#x20;
