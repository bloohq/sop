# Technology Stack

Blue is a mix of open-source software and hundreds of thousands of lines of our proprietary code.&#x20;

Our approach to making technology decisions is that SaaS (Software-as-a-Service) and managed services are better than self-hosting or building solutions in-house. This is because we will always be short of engineering time, but not of money, and so it is better to trade money for time and leverage managed services where we do not have the overhead of keeping the service running. We also gain the added advantage of leveraging the expertise of thousands of additional engineers who work at those companies and help keep the lights on.&#x20;

If everything else is equal, we default to open-source.&#x20;

## Core

### NodeJS

A JavaScript runtime built on Chrome's V8 JavaScript engine, Node.js is used for building scalable network applications.

### Rust

A multi-paradigm programming language focused on performance and safety, particularly safe concurrency, used for system development. We use this exclusively for our import/export feature to scale to hundreds of thousands of records.

### GraphQL

A query language for APIs and a runtime for executing those queries by using a type system defined for the data.

### Prisma

An open-source database toolkit, including an ORM (Object-Relational Mapping), migrations, and a query builder for Node.js and TypeScript.

### Typescript&#x20;

An open-source language which builds on JavaScript by adding static type definitions, allowing for catching errors early and providing a more robust coding experience.

### Vue

An open-source model–view–viewmodel front end JavaScript framework for building user interfaces and single-page applications.

### Vuetify

A Vue UI Library with beautifully handcrafted Material Components, aiding in the creation of responsive applications with Vue.js.

### Apollo Client

A comprehensive state management library for JavaScript that enables the management of both local and remote data with GraphQL.

## Infrastructure

### Cloudflare

This is where we host the blue.cc DNS management. We do not currently use any other Cloudflare features for our product. We're considering using the R2 storage in the future, but it's not there yet.

We use Cloudflare pages for our marketing website.

### Render

#### Frontend

Render hosts the user interface of the application, ensuring fast loading times and high availability.

#### APIs

The APIs, which are crucial for backend-frontend communication, are also hosted on Render, benefiting from its reliable infrastructure.

#### Background Workers

These are essential for executing tasks in the background, such as data processing or maintenance tasks, without affecting the user experience.

#### Redis / BullQueue

Utilized for queue management, these tools help in efficiently handling asynchronous tasks and job scheduling, which is critical for ensuring smooth operation and scalability.

### AWS&#x20;

#### Aurora

AWS Aurora is used for database services. It's a high-performance relational database that provides scalability, reliability, and availability.

#### SES (Simple Email Service)

This is used for our email automations, because we needed features that are not available in customer.io such as custom sender names, and in the future fully custom email domains.&#x20;

#### S3

Amazon's Simple Storage Service (S3) provides object storage solutions. It's used for storing and retrieving any amount of data, like user uploads, backups, or log files.

### Redislabs

Acts as a real-time engine for the Blue application.&#x20;

### Godaddy

This is where our domain is owned.&#x20;

## Services

### Stripe

This is our payment processing platform that manages the invoicing, subscriptions, and charging customers.&#x20;

### Helpscout

This our ticketing system.

Emails that we use:

* help@blue.cc
* sales@blue.cc
* partners@blue.cc
* hello@blue.cc

### Customer.io

We use Customer.io as our main email-sending service for the following:

* **Newsletter —** This newsletter announces new features and changes to the platform.
* **Cadences —** These are the series of welcome emails to company owners and new users that educate our user base on Blue features.&#x20;
* **Notification Emails —** Any email from a Blue notification to end users.&#x20;

{% hint style="danger" %}
Note that emails triggered via [email automations ](https://docs.blue.cc/platform/automations/actions/email-automations)are **not** sent via Customer.io but via AWS SES (Simple Email Service). This is due to a limitation within Customer.io where we cannot have a custom "from" name and, in the future, have emails sent from our customer's domains.&#x20;
{% endhint %}

### AppSignal

We use AppSignal for monitoring and tracking the performance of the application, as well as hosting our [Status Page.](https://status.blue.cc)



### Rewardful

This is the platform we use to manage our [Partner Program](../growth/partner-program.md) that helps third parties grow their business, while helping us grow ours. &#x20;

* [Partner Program Sign-Up](https://partners.blue.cc/signup)
* [Partner Program Login](https://partners.blue.cc/login)
* [Management Backend Login](https://app.getrewardful.com/login)

### Metabase

We host [Metabase](https://www.metabase.com/) via a managed Render.com instance, and it is directly hooked up to our production database, where it periodically clones new data so we can run analytics. This can be accessed here for users with access:

{% embed url="https://metabase.blue" %}

### Firebase (Analytics)

A service likely used for tracking and analyzing user interactions within the application. Firebase Analytics helps in understanding user behavior, which is crucial for improving user experience and guiding product development.

### ReCaptcha

Implemented for securing form entries against bots and automated abuse. ReCaptcha is a widely-used service by Google to distinguish human users from automated systems, thereby enhancing the security of online forms and processes.

## Dependencies

Dependencies are external software packages or libraries that Blue leverages to function. These external dependencies provide reusable code, functionality, or resources that help us build and maintain the Blue platform more efficiently.&#x20;

* @apollo/server
* @appsignal/nodejs
* @bull-board/api
* @bull-board/express
* @fullcalendar/core
* @fullcalendar/daygrid
* @fullcalendar/interaction
* @fullcalendar/timegrid
* @fullcalendar/vue
* @giphy/js-components
* @giphy/js-fetch-api
* @graphql-tools/graphql-file-loader
* @graphql-tools/load
* @graphql-tools/load-files
* @graphql-tools/merge
* @graphql-tools/schema
* @graphql-tools/stitch
* @joeattardi/emoji-button
* @popperjs/core
* @prisma/client
* @sindresorhus/slugify
* @stripe/stripe-js
* @tiptap/core
* @tiptap/extension-bubble-menu
* @tiptap/extension-character-count
* @tiptap/extension-code-block-lowlight
* @tiptap/extension-collaboration
* @tiptap/extension-collaboration-cursor
* @tiptap/extension-focus
* @tiptap/extension-highlight
* @tiptap/extension-link
* @tiptap/extension-mention
* @tiptap/extension-placeholder
* @tiptap/extension-superscript
* @tiptap/extension-text-align
* @tiptap/extension-typography
* @tiptap/extension-underline
* @tiptap/starter-kit
* @tiptap/suggestion
* @tiptap/vue-2
* @vue/apollo-composable
* @vue/apollo-option
* @vue/apollo-util
* @vue/composition-api
* @vueuse/components
* @vueuse/core
* ag-grid-community
* ag-grid-enterprise
* ag-grid-vue
* apollo-cache-inmemory
* apollo-client
* apollo-link
* apollo-link-context
* apollo-link-error
* apollo-link-http
* apollo-link-retry
* apollo-link-ws
* apollo-utilities
* autolinker
* aws-sdk
* axios
* bcrypt
* body-parser
* bullmq
* cheerio
* color
* content-disposition
* cookie-parser
* cors
* country-code-info
* country-code-lookup
* cropperjs
* cuid
* dataloader
* diff
* dotenv
* ejs
* elasticsearch
* express
* firebase
* firebase-admin
* flat
* flatpickr
* form-data
* fuse.js
* geoip-lite
* glightbox
* graphql
* graphql-playground-html
* graphql-tools
* graphql-ws
* graphql-middleware
* graphql-rate-limit
* graphql-redis-subscriptions
* graphql-scalars
* graphql-shield
* graphql-subscriptions
* graphql-tag
* graphql-tools
* graphql-ws
* gsap
* hex-to-rgba
* html-to-text
* http-aws-es
* ical-generator
* indicative
* ioredis
* jsonwebtoken
* knex
* langchain
* leaflet
* lodash
* markdown-it
* markdown-it-emoji
* markdown-it-link-attributes
* marked
* mjml
* moment
* moment-timezone
* mongoose
* morgan
* multer
* mysql2
* nanoid
* node-fetch
* nodemailer
* nodemailer-mailgun-transport
* oembed-parser
* papaparse
* prismjs
* pusher-js
* redis
* request-ip
* sanitize-html
* sharp
* slug
* socket.io
* source-map-support
* string-template
* stripe
* text-clipper
* ua-parser-js
* uuid
* validator
* vee-validate
* vue
* vue-apollo
* vue-chartjs
* vue-click-outside
* vue-codemirror
* vue-content-loader
* vue-cookies
* vue-datetime
* vue-dropzone
* vue-flickity
* vue-fontawesome
* vue-google-autocomplete
* vue-i18n
* vue-infinite-loading
* vue-js-modal
* vue-meta
* vue-notification
* vue-observe-visibility
* vue-property-decorator
* vue-router
* vue-swatches
* vue-sweetalert2
* vue2-leaflet
* vue2-transitions
* vuedraggable
* vuetify
* y-prosemirror
* y-websocket





