# Weekly Product Meeting

The weekly product meeting every Monday is our regular cadence for new feature reviews for the Blue platform.&#x20;

A typical agenda will focus on:

* **Product Demos**
  * Each engineer working on a feature will demo the work-in-progress
  * Q\&As from the team
  * What is left to do & open questions/difficulties that need to be discussed.&#x20;
* **Customer Feedback —** If there has been important customer feedback in the last week, or if Customer Success notices a pattern of feedback that may indicate a new feature idea or a system issue, this is raised and discussed. If required, an engineer is assigned to look into the issue.&#x20;
* Update [Known Issue List on Documentation](https://docs.blue.cc/blue-documentation/help/known-issues)
* **Planning —** Review short-term backlog and plan assignments if any team member is about to finish their work and needs new features to work on.&#x20;
* **Metrics Review:**&#x20;
  * Active companies/users both via Firebase and our own direct SQL queries to measure companies and users with a minimum level of activity within the platform.
  * [AppSignal](technology-stack.md#appsignal) Numbers
