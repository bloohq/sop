# Writing Specs

Specs help us ship better products, faster.&#x20;

In terms of deciding what feature to design, there are a few ways to go about this:

<figure><img src="../.gitbook/assets/userdesign.jpeg" alt=""><figcaption></figcaption></figure>

There are three key components to a specification:

1. **Functional Specifications:** This is how the feature works from the perspective of the end user. In this part, we can completely abstract away any technical or backend considerations.&#x20;
2. **Technical Specifications:** This describes the internal implementation. _How_ are we going to make this work?
3. **Test Cases:** How do we judge that the feature is ready to deploy?

## Functional Specifications&#x20;



* tl;dr — Explain the feature in one clear sentence.&#x20;
* Real Use Cases — We should describe at least two real use cases for customers in different industries.&#x20;
* Press Release — concept of working backwards from Amazon. In our case, we write a newsletter and blog post if its a major feature.
* Description of V1 — A detailed description of the V1. We describe the functionality of the software in human language, that anyone can understand. Add lots of details.&#x20;
* Open Issues — This is a list of things that we have not yet worked out.&#x20;
* Non-Goals — What are we specifically **not** doing in this feature?&#x20;
* Future version — a description of what we will do in the future, but now not. This may result in changes.&#x20;

## Technical Specifications&#x20;

Engineering approach — The engineer that is going to be working on this feature will write their technical description of the approach to the feature.&#x20;



## Test Cases

We do not have to write the test cases ourselves. We can upload the designs and functional specification to ChatGPT and get back a list of test case scenarios that we can run through, then we can use our brains to expand on these test cases and ensure that things are working.&#x20;

