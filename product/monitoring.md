# Monitoring

We use AppSignal for system monitoring aned for our [Status Page](https://status.blue.cc).

<div data-full-width="true">

<figure><img src="../.gitbook/assets/CleanShot 2024-01-24 at 22.26.02@2x.png" alt=""><figcaption></figcaption></figure>

</div>

One of the most important things to keep our eye on is the issues list, which should be our priority when we are looking to optimise our systems.&#x20;

<div data-full-width="true">

<figure><img src="../.gitbook/assets/CleanShot 2024-01-24 at 22.27.50@2x.png" alt=""><figcaption></figcaption></figure>

</div>
