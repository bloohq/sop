# Banking

Our banking and payments are done via the following:

* [**Stripe**](https://stripe.com) — This is our payment processor for all payments from customers to Blue.
* [**Mercury**](https://mercury.com) — This is our central bank account that receives funds from Stripe. We have both a checking and savings account.&#x20;
* [**Wise**](https://wise.com) — We use Wise for international transfers and paying out revenue share payments to [partners](../growth/partner-program.md).&#x20;
* **Petty Cash Account** — This is used for operational transfers, certain team payments, and small reimbursements. &#x20;
