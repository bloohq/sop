# Free Cash Flow Tracking

We create and track FCF (Free Cash Flow) projects for the next twelve months. This is done here:

{% embed url="https://docs.google.com/spreadsheets/d/1sdoZIPcpn1NEYwZ9RUbvSl8-GwxwkQ5dYF0cBs-_sfw/edit#gid=0" %}
Private Free Cash Flow Tracking Sheet
{% endembed %}

This sheets tracks:

* **Income:** Total cash in from subscription and one-off payments.&#x20;
* **COGS:** Direct technology costs of running the Blue platform
* **Staff:** Costs for the various full-time team members
* **General Expenses:** Other expenses and non-platform related technology costs.&#x20;
* **Gross Profit:** Income - COGS
* **Net Profit:** Gross Profit - Staff - General Expenses
* **Cumulative Cash Flow:** The accumulated cash flow position since the start of the year.&#x20;

Shareholder loans and contributions are tracked here:

{% embed url="https://docs.google.com/spreadsheets/d/1-QQ10syDSowMOEFEGz5XV5Ruh9XUi_PUkU5xSCcxfHg/edit#gid=0" %}
Private Loans & Contributions Tracking Sheet
{% endembed %}

